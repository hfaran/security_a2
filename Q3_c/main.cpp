#include <iostream>
#include <fstream>
#include <ctime>
//#include <stdio.h>
#include <string.h>
#include "crc.h"

using namespace std;

#define N 8
#define ALPHANUMERIC "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
#define AN_LEN strlen(ALPHANUMERIC)
#define STEP (100*1000*1000)
#define LOOPLIM 1000

unsigned long calculate_crc(crc_t* crc, unsigned char * str, size_t len) {
    *crc = crc_init();
    *crc = crc_update(*crc, str, len);
    *crc = crc_finalize(*crc);

    return (unsigned long)(*crc);
}

void make_string(unsigned char * str) {
    for (int i=0; i<N; i++) {
        //str[i] = ALPHANUMERIC[rand() % AN_LEN];
        str[i] = rand() % 127;
    }
}


bool next_string(unsigned char * str) {
    str[0] += 1;
    for (int i=0; i<N; i++) {
        if (str[i] > 127) {
            if (i==N-1) {
                return false;
            }
            str[i] = 0;
            str[i+1] += 1;
        } else {

            break;
        }
    }
    return true;
}


void print_char_array_hex(unsigned char * arr, int len) {
    for (int i=0; i<len; i++) {
        std::printf("%x ", (unsigned)arr[i]);
    }
}

void check_collision(crc_t crc, unsigned char *x, unsigned char *y) {
    unsigned long xcrc;
    unsigned long ycrc;

    xcrc = calculate_crc(&crc, x, N);
    ycrc = calculate_crc(&crc, y, N);
    if (xcrc == ycrc) {
        std::printf(" x: ");
        print_char_array_hex(x, N);
        std::printf(" y: ");
        print_char_array_hex(y, N);
        std::printf("xcrc: 0x%lx ", xcrc);
        std::printf("ycrc: 0x%lx\n", ycrc);
    }
}



void find_collision_random() {
    crc_t crc;
    unsigned char x[N+1] = {0};
    unsigned char y[N+1] = {0};
    std::time_t result;

    x[N] = '\0';
    y[N] = '\0';
    for (long long _i=0; _i<LOOPLIM; _i++) {
        result = std::time(nullptr);
        for (long long i=0; i<STEP; i++) {
            make_string(x);
            make_string(y);
            check_collision(crc, x, y);
        }
        std::printf("Iterations: %lld; its/sec: %lf\n",
                    (_i+1)*STEP,
                    STEP/(double)(std::time(nullptr)-result));
    }
}


void find_collision_next() {
    crc_t crc;
    unsigned char x[N] = {0};
    unsigned char y[N] = {0};
    std::time_t result;
    result = std::time(nullptr);
    long long _i = 0;

    x[N] = '\0';
    y[N] = '\0';

    check_collision(crc, x, y);
    ++_i;

    //while (next_string(x)) {
    while (next_string(y)) {
        check_collision(crc, x, y);
        ++_i;
        if (_i%STEP == 0) {
            std::printf("Iterations: %lld; its/sec: %lf; ",
                        _i,
                        STEP/(double)(std::time(nullptr)-result)
            );
            std::printf(" x: ");
            print_char_array_hex(x, N);
            std::printf(" y: ");
            print_char_array_hex(y, N);
            std::printf("\n");
            result = std::time(nullptr);
        }
    }

    //}
}


int main() {
    //find_collision_random();
    find_collision_next();
    //crc_t crc;
    //check_collision(crc, "000bUQXV", "0003704J");
    return 0;
}
