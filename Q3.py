#!/usr/bin/env pypy

import random
from itertools import product
from string import ascii_letters
from string import digits
from time import time

from concurrent.futures import ProcessPoolExecutor

from PyCRC.CRC32 import CRC32


CHARS = ascii_letters + digits


def make_string(n):
    return "".join(random.choice(CHARS) for i in range(n))


def find_collision_random(n=None, x=None):
    assert n is not None or x is not None
    if x is not None:
        n = len(x)
        static_x = True
    else:
        static_x = False
    print("Finding CRC32 collisions for n={} and x={}...".format(n, x))

    icount = 0
    step = 1000000
    crc32 = CRC32()
    while 1:
        start = time()
        for i in range(step):
            x = make_string(n) if static_x is False else x
            y = make_string(n)
            crc32_x = crc32.calculate(x)
            crc32_y = crc32.calculate(y)
            if crc32_x == crc32_y and x != y:
                print("x: {}, y: {}".format(x, y))
        icount += 1
        print("Iterations: {}; its/sec: {}".format(
            icount*step, step/(time() - start)
        ))


def find_collision_seq(n=None, x=None):
    assert n is not None or x is not None
    if x is not None:
        n = len(x)
        x_range = [x]
    else:
        x_range = product(CHARS, repeat=n)
    print("Finding CRC32 collisions for n={} and x={}...".format(n, x))

    icount = 0
    step = len(CHARS)**n
    crc32 = CRC32()
    for x in x_range:
        x = "".join(x)
        start = time()
        for y in product(CHARS, repeat=n):
            y = "".join(y)
            crc32_x = crc32.calculate(x)
            crc32_y = crc32.calculate(y)
            if crc32_x == crc32_y and x != y:
                print("x: {}, y: {}".format(x, y))
        icount += 1
        print("Iterations: {}; its/sec: {}".format(
            icount*step, step/(time() - start)
        ))


# def find_collisions():
#     for n in range(1,9):
#         find_collision(n=n)


if __name__ == '__main__':
    find_collision_seq(n=5)
