from collections import defaultdict
from functools import partial
from math import log10
import os
from string import ascii_uppercase


def repeated_substring(s, n):
    d = defaultdict(int)
    for p in xrange(len(s)-n+1):
        sub = s[p:p+n]
        d[sub] += 1
    return d


def repeated_ngraphs(l, n):
    d = defaultdict(int)
    for p in xrange(len(l)):
        sub = "".join(l[p:p+n])
        d[sub] += 1
    return d


def sanitize_text(s):
    alphaset = set(ascii_uppercase)
    return "".join(c for c in s.strip().upper() if c in alphaset)


def fetch_online_corpora():
    texts = []
    # We use the freely available Moby Dick
    # http://www.gutenberg.org/cache/epub/2701/pg2701.txt"
    dirs = ["/tmp/Security_A2/txt/misc"]
    # Some corpora from the American National Corpus:
    # http://www.anc.org/data/masc/downloads/data-download/
    dirs.extend([
        "/tmp/Security_A2/txt/masc_500k_texts/written/"+d for d in
        ["fiction", "ficlets", "jokes", "non-fiction", "essays"]
    ])
    for P in dirs:
        for fn in os.listdir(P):
            with open(os.path.join(P, fn)) as f:
                texts.append(f.read().strip())
    return texts


def compile_corpora():
    texts = fetch_online_corpora()
    alphaset = set(ascii_uppercase)
    text = "".join(c for text in texts for c in text
                   if c in alphaset)
    return text


def quadgram_fitness_from_text(text):
    count = repeated_substring(text, 4)
    sum_count = float(sum(count.values()))
    qfm = {k: log10(v/sum_count) for k, v in count.items()}
    # Return a floor value to use as default for quadgrams that
    #  don't exist in the corpus
    floor = log10(0.01/sum_count)
    return qfm, floor


def fitness_score(text, quadgram_fitness_map, floor):
    qfm = quadgram_fitness_map
    count = repeated_substring(sanitize_text(text), 4)
    return sum(qfm.get(sub, floor)*v for sub,v in count.iteritems())


def test__fitness_score():
    qfm, floor = quadgram_fitness_from_text(compile_corpora())
    fs = partial(fitness_score, quadgram_fitness_map=qfm, floor=floor)
    for eng, gar in [("THIS IS A EMERGENCY THIS IS NOT A DRILL I REPEAT",
                      "SIHTHTBAPBWMCPDXSIHTHTOPQDBTFOOFUBUKDQ")]:
        engs = fs(eng)
        gars = fs(gar)
        assert engs > gars


if __name__ == '__main__':
    test__fitness_score()
