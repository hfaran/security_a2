import math
import random

from random import shuffle, randint
from string import ascii_uppercase
from time import time

from Q2_fitness import fitness_score, compile_corpora, \
    quadgram_fitness_from_text, sanitize_text


def gen_rand_playfair_key():
    """Generate a random key for the Playfair cipher

    :rtype: str

    >>> res = gen_rand_playfair_key()
    >>> assert "J" not in res
    >>> assert len(res) == 25
    """
    key = set(ascii_uppercase)
    key.remove("J")
    key = list(key)
    shuffle(key)
    return "".join(key)


def swap_key(key):
    """Swap two characters in key (obviously not in place)

    :param str key: Key to modify
    :return: New key
    """
    key = list(key)
    a, b = randint(0,len(key)-1), randint(0,len(key)-1)
    if a == b:
        a = (b+len(key)/2)%len(key)
    temp = key[a]
    key[a] = key[b]
    key[b] = temp
    return "".join(key)


def swap_row(key):
    a = randint(0,4)
    b = randint(0,4)
    key = ["".join(s) for s in zip(*(iter(key),)*5)]
    temp = key[a]
    key[a] = key[b]
    key[b] = temp
    return "".join(key)


def key_shuffle(key):
    s = list(key)
    shuffle(s)
    return "".join(s)


def reverse_key(key):
    return "".join(reversed(key))


def key_change(key):
    funcs = [
        swap_key,
        swap_row,
        key_shuffle,
        reverse_key
    ]
    func = random.choice(funcs)
    return func(key)


def decipher_pair(pair, key):
    a, b = pair
    a_index = key.index(a)
    ax, ay = (a_index/5, a_index%5)
    b_index = key.index(b)
    bx, by = (b_index/5, b_index%5)
    # If the rows are the same, we shift back one each column
    #  when deciphering
    if ax == bx:
        return "{}{}".format(key[ax*5+(ay-1)%5], key[bx*5+(by-1)%5])
    # Same as above except for columns this time
    elif ay == by:
        return "{}{}".format(key[((ax-1)%5)*5+ay], key[((bx-1)%5)*5+by])
    else:
        return "{}{}".format(key[ax*5+by], key[bx*5+ay])


def decipher(ct, key):
    ct = sanitize_text(ct).upper()
    key = key.upper()

    # If odd text, add an X to make even
    if len(ct) % 2 == 1:
        ct += 'X'

    return "".join(decipher_pair(pair, key) for pair in zip(*(iter(ct),)*2))


def playunfair(ct, temp=20.0, step=0.2, count=10000):
    qfm, floor = quadgram_fitness_from_text(compile_corpora())
    e = math.e

    best_key = parent_key = gen_rand_playfair_key()
    best_fitness = parent_fitness = None

    T = temp
    while T >= 0:
        print("Temperature: {}".format(T))
        print("Fitness: {}".format(parent_fitness))
        print("Key: {}".format(parent_key))
        print("Plaintext: {}".format(decipher(ct, parent_key)))
        start = time()
        for i in xrange(count):
            new_key = key_change(parent_key)
            text = decipher(ct, new_key)
            fitness = fitness_score(text, qfm, floor)
            if parent_fitness is None:
                parent_fitness = fitness
                parent_key = new_key
                continue
            dF = fitness - parent_fitness
            if dF > 0.0:
                best_fitness = parent_fitness = fitness
                best_key = parent_key = new_key
            else:
                prob = e**(dF/T)
                if prob > random.uniform(0, 1):
                    parent_fitness = fitness
                    parent_key = new_key
        print("Keys/s: {:.2f}".format(count/(time() - start)))
        print("")
        T -= step

    print("Best Fitness: {}".format(best_fitness))
    print("Best Key: {}".format(best_key))

    print(decipher(ct, best_key))
    return parent_key
