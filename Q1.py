from hashlib import md5
from collections import defaultdict
from string import maketrans

sn = 12563110
md5_sid = md5(str(sn)).hexdigest()

ct = ("HXKKVDYTRDIIUKAXZJRDIIUPXLPDHQZJDMNYHXKCXYNELURNRDII"
      "UUZTHXKRDUADCIUYMNLLDQKIUXLRDIIUAHNJXCADCAHNTVUYMNKCYDIA"
      "HNTYUJDZHDUYTRDIIUHNLNZAADUIQKNQIRDIIUADAHNIXRHNLTNLMXZJIUA"
      "HDIHDQKNXZCURATDAPQAHNSNEAXZUTYUVNYUAPUJNZTAHNDLTRLDUSUZTHDD"
      "TAHUAHNHUTVDYZDZHXKAYUMNLKUZTAHNYXZJRDIIUKNRQYNTPWUCXZNRHUXZR"
      "DIIUYNIUXZNTXZHXKEDRSNATDAHNYNAQYZNTADHXKHDINUAPUJNZTDZFQZNAHNA"
      "VDAVDZTXZHXKCXCAWKNRDZTWNUYTDAKTDAYTDADZNAHYNNCDQYAVDTDARDIIUU"
      "ZTZDAHXZJMNYWZDAUPLNDRRQYYNTXZAHNKHXYNQZAXLIY")


rev_val_sort = lambda x: sorted(x, reverse=True, key=lambda t: t[1])


def repeated_substring(s, n):
    d = defaultdict(int)
    for p in xrange(len(s)):
        sub = s[p:p+n]
        d[sub] += 1
    return d


def top_n_words(d, n):
    """

    :param dict d: Words as keys, with their frequency as values
    :param int n: The top n amount
    :rtype: list
    """
    return sorted(d.items(), key=lambda x: x[1], reverse=True)[:n]


def similar_words(worddict_len, word):
    for w in worddict_len[len(word)]:
        mono_key = {}
        for a, b in zip(word.lower(), w.lower()):
            if a in mono_key:
                if not b == mono_key[a]:
                    break
            else:
                mono_key[a] = b
        else:
            yield w

def create_worddict_len(wordlist):
    worddict_len = defaultdict(list)
    for word in wordlist:
        worddict_len[len(word)].append(word)
    return worddict_len

def decipher(cipher_map, ct):
    decipher_table = maketrans("".join(cipher_map.keys()),
                               "".join(cipher_map.values()).lower())
    deciphered = ct.translate(decipher_table)
    return deciphered

def test__similar_words():
    wdl = {5: ["COMMA"]}
    assert list(similar_words(wdl, "FOJJH")) == ["COMMA"]

if __name__ == '__main__':
    test__similar_words()
