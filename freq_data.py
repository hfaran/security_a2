# https://www.englishclub.com/vocabulary/common-words-100.htm
hundred_most_common = """
the
be
to
of
and
a
in
that
have
I
it
for
not
on
with
he
as
you
do
at
this
but
his
by
from
they
we
say
her
she
or
an
will
my
one
all
would
there
their
what
so
up
out
if
about
who
get
which
go
me
when
make
can
like
time
no
just
him
know
take
person
into
year
your
good
some
could
them
see
other
than
then
now
look
only
come
its
over
think
also
back
after
use
two
how
our
work
first
well
way
even
new
want
because
any
these
give
day
most
us
""".strip().splitlines()


# String sourced from http://www.math.cornell.edu/~mec/2003-2004/cryptography/subs/frequencies.html
freq_table = {l[2].upper(): float(l[3]) for l in map(str.split, """
E	21912	 	E	12.02
T	16587	 	T	9.10
A	14810	 	A	8.12
O	14003	 	O	7.68
I	13318	 	I	7.31
N	12666	 	N	6.95
S	11450	 	S	6.28
R	10977	 	R	6.02
H	10795	 	H	5.92
D	7874	 	D	4.32
L	7253	 	L	3.98
U	5246	 	U	2.88
C	4943	 	C	2.71
M	4761	 	M	2.61
F	4200	 	F	2.30
Y	3853	 	Y	2.11
W	3819	 	W	2.09
G	3693	 	G	2.03
P	3316	 	P	1.82
B	2715	 	B	1.49
V	2019	 	V	1.11
K	1257	 	K	0.69
X	315	 	X	0.17
Q	205	 	Q	0.11
J	188	 	J	0.10
Z	128	 	Z	0.07
""".strip().splitlines())}

def normalize_table_values(d):
    max_val_d = float(max(d.values()))
    norm_d = {k: v/max_val_d for k, v in d.items()}
    return norm_d

norm_freq_table = normalize_table_values(freq_table)
