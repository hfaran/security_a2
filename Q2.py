#!/usr/bin/env pypy

from Q2_sima import playunfair

with open("Q2_cipher.txt") as f:
    ct = f.read().strip()

if __name__ == '__main__':
    playunfair(ct)
