#include <iostream>
#include <fstream>
#include "crc.h"

using namespace std;

#define N 8
#define STEP (100*1000*1000)


void print_char_array_hex(unsigned char * arr, int len) {
    for (int i=0; i<len; i++) {
        std::printf("%x ", (unsigned)arr[i]);
    }
}


unsigned long calculate_crc(crc_t* crc, unsigned char * str, size_t len) {
    *crc = crc_init();
    *crc = crc_update(*crc, str, len);
    *crc = crc_finalize(*crc);

    return (unsigned long)(*crc);
}


bool next_string(unsigned char * str) {
    str[0] += 1;
    for (int i=0; i<N; i++) {
        if (str[i] > 127) {
            if (i==N-1) {
                return false;
            }
            str[i] = 0;
            str[i+1] += 1;
        } else {

            break;
        }
    }
    return true;
}

void check_collision(crc_t crc, unsigned long xcrc, unsigned char *y) {
    unsigned long ycrc;

    ycrc = calculate_crc(&crc, y, N);
    if (xcrc == ycrc) {
        std::printf(" y: ");
        print_char_array_hex(y, N);
        std::printf("xcrc: 0x%lx ", xcrc);
        std::printf("ycrc: 0x%lx\n", ycrc);
    }
}

void find_collision_next() {
    crc_t crc;
    unsigned char x[] = "8DFC54E9FDB8B16F7CAF10173D89718A";
    unsigned char y[N] = {0};
    size_t xlen = strlen((char*)x);
    unsigned long xcrc = calculate_crc(&crc, x, xlen);
    std::time_t result;
    result = std::time(nullptr);
    long long _i = 0;

    check_collision(crc, xcrc, y);
    ++_i;

    while(next_string(y)) {
        check_collision(crc, xcrc, y);
        ++_i;
        if (_i%STEP == 0) {
            std::printf("Iterations: %lld; its/sec: %lf; ",
                        _i,
                        STEP/(double)(std::time(nullptr)-result)
            );
            std::printf(" x: ");
            print_char_array_hex(x, xlen);
            std::printf(" y: ");
            print_char_array_hex(y, N);
            std::printf("\n");
            result = std::time(nullptr);
        }
    }
}


int main() {
    find_collision_next();
    return 0;
}
